# MIME Type - Coding Games

Link do exercicio: https://www.codingame.com/ide/puzzle/mime-type

Resolvido em python3

# The Goal
MIME types are used in numerous internet protocols to associate a media type (html, image, video ...) with the content sent. The MIME type is generally inferred from the extension of the file to be sent.

You have to write a program that makes it possible to detect the MIME type of a file based on its name.
# Rules
You are provided with a table which associates MIME types to file extensions. You are also given a list of names of files to be transferred and for each one of these files, you must find the MIME type to be used.

The extension of a file is defined as the substring which follows the last occurrence, if any, of the dot character within the file name.
If the extension for a given file can be found in the association table (case insensitive, e.g. TXT is treated the same way as txt), then print the corresponding MIME type. If it is not possible to find the MIME type corresponding to a file, or if the file doesn’t have an extension, print UNKNOWN.

# Considerações
A proposta do problema é dada uma lista de tipos de arquivos e uma lista de arquivos verificar se as extensões são validas.

O problema ja nos fornecia o metodo de entrada de dados e os loops para armazenar os dados então fiz as verificações conforme as regras fornecidas, tive um pouco de dificuldade na manipulação do dicionário mas com algumas pesquisas sanei minhas duvidas e estou adicionando 2 arquivos pois nas resoluções dos exemplos o output apresentava somente o resultado da verificação e no exercicio solicita que após inserir o nome do arquivo realizar a verificação ficando 
 - mime_desafio _o codigo com o output exibido linha por linha_
 - mime_enunciado _o codigo com output exibido ao final somente o resultado da verificação_