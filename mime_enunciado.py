import os
import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

n = int(input())  # Número de elementos que serão reconhecidos
q = int(input())  # Número de arquivos que serão analizados.

mime_type_map = {}  # dicionario onde será armazenado a extensão(key) e tipo de arquivo(valor)
file_type_map = []  # lista onde será armazenado o resultado da verificação de extensão

""" Loop para mapear as extensões conhecidas, em primeiro momento realizar a inserção da extensão
e tipo de arquivo onde o mesmo será separado através do espaço e será armazenado no dicionário "mime_type_map"
"""
for i in range(n):
    # ext: extensão do arquivo.
    # mt: MIME type.
    ext, mt = input().split()  # One file extension per line.
    mime_type_map[ext.lower()] = mt  #adiciona no dicionário

"""Loop para mapear os nomes dos arquivos utilizando o "." como separação da extensão e utlizando o "[-1]" para
começar do final do nome o ".lower" para deixar padronizado e por ultimo realizar a validação adicionando o resultado
na lista "file_type_map"
"""
for i in range(q):
    filename = input()  # One file name per line.
    if not "." in filename:
        print("UNKNOWN")
        continue
    file_ext = filename.split(".")[-1].lower()
    if mime_type_map.get(file_ext):
        file_type_map.append(mime_type_map[file_ext])
    else:
        file_type_map.append("UNKNOWN")

"""Loop para exibir o resultado dos arquivos inseridos conforme o output do exercicio
"""
for i in range(len(file_type_map)):
    print(file_type_map[i])


os.system("pause")
