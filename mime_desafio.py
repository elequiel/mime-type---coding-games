import sys
import math

n = int(input())  # Number of elements which make up the association table.
q = int(input())  # Number Q of file names to be analyzed.
mime_type_map = {}

mime_type_map = {}  # dicionario onde será armazenado a extensão(key) e tipo de arquivo(valor)

""" Loop para mapear as extensões conhecidas, em primeiro momento realizar a inserção da extensão
e tipo de arquivo onde o mesmo será separado através do espaço e será armazenado no dicionário "mime_type_map"
"""
for i in range(n):
    # ext: extensão do arquivo.
    # mt: MIME type.
    ext, mt = input().split()  # One file extension per line.
    mime_type_map[ext.lower()] = mt  #adiciona no dicionário
   
"""Loop para mapear os nomes dos arquivos utilizando o "." como separação da extensão e utlizando o "[-1]" para
começar do final do nome o ".lower" para deixar padronizado e por ultimo realizar a validação e exibe o resultado"
"""
for i in range(q):
    filename = input()  # One file name per line.
    if not "." in filename:
        print("UNKNOWN")
        continue
    file_ext = filename.split(".")[-1].lower()
    if mime_type_map.get(file_ext):
        print(mime_type_map[file_ext])
    else:
        print("UNKNOWN")
